package ch.bbbaden.m120.la8054.toDoIt.viewModel;

import ch.bbbaden.m120.la8054.toDoIt.Main;
import ch.bbbaden.m120.la8054.toDoIt.model.Project;
import ch.bbbaden.m120.la8054.toDoIt.model.ProjectModel;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ProjectListVM implements PropertyChangeListener {
    private ObservableList<Project> projects = FXCollections.observableArrayList();
    private BooleanProperty selected = new SimpleBooleanProperty(true);
    private StringProperty projectTitle = new SimpleStringProperty();
    private IntegerProperty projectTaskSize = new SimpleIntegerProperty();
    private Project selectedProject;
    private ProjectModel model;
    private Main main;

    public ProjectListVM(Main main, ProjectModel model) {
        this.main = main;
        this.model = model;
    }
    public void actEdit() {
        //nope
    }
    public void actRemove() {
        model.removeProject(selectedProject);
    }

    public BooleanProperty getSelectedProperty() {
        return selected;
    }

    public void addProject() {
        main.showProjectForm(selectedProject);
    }

    public void showTask() {//remove once done with prototyping
        main.showTaskForm();
    }

    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "projects":
                this.projects.setAll(model.getProjects());
                break;
        }
    }
    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
        model.setSelectedProject(selectedProject);
        if (selectedProject != null) {
            projectTitle.setValue(selectedProject.getTitle());
            projectTaskSize.setValue(selectedProject.getNumberOfTasks());
            selected.setValue(false);
        } else {
            projectTitle.setValue("");
            projectTaskSize.setValue(0);
            selected.setValue(true);
        }
    }

    public ObservableValue<String> getProjectTitle() {
        return projectTitle;
    }

    public IntegerProperty getProjectTaskSize() {
        return projectTaskSize;
    }

    public ObservableList getProjects() {
        return projects;
    }

    public void addTask() {
    }
}
