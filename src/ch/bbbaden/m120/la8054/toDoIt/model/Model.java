package ch.bbbaden.m120.la8054.toDoIt.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

public class Model implements ProjectModel, TaskModel {
    protected final PropertyChangeSupport changes = new PropertyChangeSupport(this);

    //Properties
    List<Project> projects;
    List<Task> tasks;
    Project selectedProject;

    List<Project> oldProj;
    List<Task> oldTasks;
    Project oldSelectedProject;


    public Model() {
        projects = new ArrayList<>();
    }

    @Override
    public List<Project> getProjects() {
        return projects;
    }


    @Override
    public void addProject(Project p) {
        projects.add(p);
        updateProjects();
    }

    @Override
    public void saveProject(Project p) {
        //nope
    }

    @Override
    public void removeProject(Project p) {
        projects.remove(p);
        updateProjects();
    }

    @Override
    public void setSelectedProject(Project p) {
        selectedProject = p;
        updateSelectedProject();
    }


    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }


    @Override
    public void addTask(Project p, Task newTask) {
        p.addTask(newTask);
        updateTasks();
    }

    @Override
    public void editTask(Project p, Task newTask) {
        //nope
    }

    @Override
    public void removeTask(Project p, Task task) {
        p.addTask(task);
        updateTasks();
    }

    @Override
    public void deleteAllTasks(Project p) {
        p.removeAllTasks();
        updateTasks();
    }

    @Override
    public List<Task> getAllTasks(Project p) {
        return p.getAllTasks();
    }

    @Override
    public Project getSelectedProject() {
        return selectedProject;
    }


    private void updateProjects() {
        changes.firePropertyChange("projects", oldProj, projects);
        oldProj = new ArrayList<>(projects);
    }

    private void updateTasks() {
        changes.firePropertyChange("tasks", oldTasks, projects);
        oldTasks = new ArrayList<>(tasks);
    }

    private void updateSelectedProject() {
        changes.firePropertyChange("selectedProject", oldSelectedProject, selectedProject);
        oldSelectedProject = selectedProject;
    }
}
