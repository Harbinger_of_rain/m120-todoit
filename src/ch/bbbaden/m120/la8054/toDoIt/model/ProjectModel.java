package ch.bbbaden.m120.la8054.toDoIt.model;

import java.beans.PropertyChangeListener;
import java.util.List;

public interface ProjectModel {
    public List<Project> getProjects();
    public void addPropertyChangeListener(PropertyChangeListener l);
    public void removePropertyChangeListener(PropertyChangeListener l);
    public void addProject(Project p);
    public void saveProject(Project p);
    public void removeProject(Project p);

    public void setSelectedProject(Project p);
}
