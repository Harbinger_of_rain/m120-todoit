package ch.bbbaden.m120.la8054.toDoIt.model;

import java.time.LocalDate;

public class Task {
    private String title;
    private String description;
    private LocalDate dueDate;
    private State state;

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
