package ch.bbbaden.m120.la8054.toDoIt.model;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private String title;
    private List<Task> tasks;

    public Project(String title) {
        this.title = title;
        tasks = new ArrayList<>();
    }

    public int getNumberOfTasks() {
        return tasks.size();
    }
    public void addTask(Task task) {
        tasks.add(task);
    }
    public boolean removeTask(Task task) {
        return tasks.remove(task);
    }
    public void removeAllTasks() {
        tasks.clear();
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title;
    }

    public List<Task> getAllTasks() {
        return tasks;
    }
}
